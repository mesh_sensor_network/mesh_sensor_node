// rf22_mesh_client.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, routed reliable messaging client
// with the RHMesh class.
// It is designed to work with the other examples rf22_mesh_server*
// Hint: you can simulate other network topologies by setting the 
// RH_TEST_NETWORK define in RHRouter.h

// Mesh has much greater memory requirements, and you may need to limit the
// max message length to prevent wierd crashes
#define RH_MESH_MAX_MESSAGE_LEN 50

#include <RHMesh.h>
#include <RH_RF22.h>
#include <SPI.h>
#include <DHT.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_SI1145.h>

// In this small artifical network of 4 nodes,
#define CLIENT_ADDRESS 1
#define SERVER1_ADDRESS 2
#define SERVER2_ADDRESS 3
#define SERVER3_ADDRESS 4

#define DHT11_PIN 12
#define DHTTYPE DHT11

#define ONE_WIRE_BUS 11

#define VBATPIN A7

// Singleton instance of the radio driver
RH_RF22 driver(10, A5);

// Class to manage message delivery and receipt, using the driver declared above
RHMesh manager(driver, CLIENT_ADDRESS);

// Class to manage the DHT11 sensor
DHT dht(DHT11_PIN, DHTTYPE);
Adafruit_SI1145 uv = Adafruit_SI1145();

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(115200);
  if (!manager.init()  || !uv.begin())
    Serial.println("init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
  sensors.begin();
  dht.begin();
}

// Dont put this on the stack:
uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];

uint16_t messageNum = 1;

union Data {
  float a;
  uint8_t bytes[4];
};

void loop() {
  Serial.println("Reading Temp and Humidity");

  union Data temp, humidity, uvLvl, batLvl;

  batLvl.a = (((analogRead(VBATPIN) * 2) * 3.3) / 1024);
  uvLvl.a = uv.readUV()/100.0;
  sensors.requestTemperatures();
  temp.a = sensors.getTempCByIndex(0);
  humidity.a = dht.readHumidity();

  /*
  Serial.println(temp.a);
  Serial.println(humidity.a);
  Serial.println(uvLvl.a);
  */

  uint8_t data[18];

  memcpy(&data[0], &messageNum, sizeof(messageNum));
  memcpy(&data[2], &temp.bytes, sizeof(temp.bytes));
  memcpy(&data[6], &humidity.bytes, sizeof(humidity.bytes));
  memcpy(&data[10], &uvLvl.bytes, sizeof(uvLvl.bytes));
  memcpy(&data[14], &batLvl.bytes, sizeof(batLvl.bytes));

  /*
  for(int i = 0; i < sizeof(data); i++) {
    Serial.print(data[i], HEX);
    Serial.print(":");
  }
  Serial.println();
  */
  
  Serial.println("Sending to manager_mesh_server3");
    
  // Send a message to a rf22_mesh_server
  // A route to the destination will be automatically discovered.
  if (manager.sendtoWait(data, sizeof(data), SERVER3_ADDRESS) == RH_ROUTER_ERROR_NONE) {
    // It has been reliably delivered to the next node.
    // Now wait for a reply from the ultimate server
    if(messageNum >= 65535) {
      messageNum = 0;
    }
    messageNum++;
    uint8_t len = sizeof(buf);
    uint8_t from;
  }
  else
     Serial.println("No intermediate nodes");
}

